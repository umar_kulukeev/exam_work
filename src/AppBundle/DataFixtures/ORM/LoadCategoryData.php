<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoryData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1
            ->setName('Restaurants');

        $manager->persist($category1);

        $category2 = new Category();
        $category2
            ->setName('Cafes');

        $manager->persist($category2);

        $category3 = new Category();
        $category3
            ->setName('Pubs');

        $manager->persist($category3);

        $category4 = new Category();
        $category4
            ->setName('Taverns');

        $manager->persist($category4);

        $manager->flush();
    }
}
