<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PictureAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('picture')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('picture')
            ->add('user', EntityType::class, [
                'class' => 'AppBundle\Entity\User'
            ])
            ->add('restaurant', EntityType::class, [
                'class' => 'AppBundle\Entity\Restaurant'
            ])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id')
            ->add('name')
            ->add('picture')
            ->add('imageFile', FileType::class)
            ->add('user', EntityType::class, [
                'class' => 'AppBundle\Entity\User'
            ])
            ->add('restaurant', EntityType::class, [
                'class' => 'AppBundle\Entity\Restaurant'
            ])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('user', EntityType::class, [
                'class' => 'AppBundle\Entity\User'
            ])
            ->add('restaurant', EntityType::class, [
                'class' => 'AppBundle\Entity\Restaurant'
            ])
            ->add('picture', null, array('template' => 'AppBundle:Image:list_image.html.twig'))
        ;
    }
}
