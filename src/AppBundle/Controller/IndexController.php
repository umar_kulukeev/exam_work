<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Restaurant;
use AppBundle\Form\PlaceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET","HEAD"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        $restaurants = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurant')
            ->findAll();

        return $this->render('AppBundle:Basic:index.html.twig', array(
            'restaurants' => $restaurants
        ));
    }

    /**
     * @Route("/restaurant/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(int $id) {
        $restaurant = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurant')
            ->find($id);

        return $this->render('AppBundle:Basic:details.html.twig', array(
            'restaurants' => $restaurant
        ));
    }

    /**
     * @Route("/places/create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $restaurant = new Restaurant();

        $form = $this->createForm(PlaceType::class, $restaurant);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $restaurant = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($restaurant);
            $em->flush();

            return $this->redirectToRoute('AppBundle:Basic:details.html.twig', [
                "id" => $restaurant->getId()
            ]);
        }

        return $this->render('AppBundle:Basic:create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}
